const express = require('express');
const app = express();
const session = require('express-session')

const { user_game, user_game_biodata } = require('./models');

app.use(express.json());
app.use(session({
  secret: 'aditya',
  name: 'uniqueSessionID',
  saveUninitialized: false
}))

app.use(express.urlencoded({
  extended: false
}));

app.use(express.static('public'));
app.set('view engine', 'ejs');

app.get('/login', (req, res) => {
    res.render('login');
  })
  
  app.post('/login', (req, res) => {
    const userName = req.body.username;
    const password = req.body.password;
  
    user_game.findAll({
      where: {
        email: userName,
        password: password
      }
    }).then(userGame => {
      if (userGame.length === 0) {
        res.redirect(301, '/login')
      }
      req.session.loggedIn = true;
      res.redirect('/dashboard');
    })
  })
  
  app.get('/dashboard', (req, res) => {
    if (req.session.loggedIn) {
      user_game.findAll().then(userGame => {
        res.render('dashboard', { userGame });
      })
    } else {
      res.redirect('/login')
    }
  })
  
  app.get('/user/add', (req, res) => {
    res.render('userAddView');
  })
  
  app.post('/user/save', (req, res) => {
    user_game.create({
      name: req.body.name,
      email: req.body.email,
      credit: req.body.credit
    }).then(userGame => {
      user_game_biodata.create({
        user_game_id: userGame.id,
        alamat: req.body.alamat,
        kelamin: req.body.kelamin
      }).then(biodata => res.redirect(301, '/dashboard'))
    })
  })
  
  app.get('/user/delete/:id', (req, res) => {
    const userId = req.params.id;
  
    user_game_biodata.destroy({
      where: {
        user_game_id: userId
      }
    }).then(biodata => {
      user_game.destroy({
        where: {
          id: userId
        }
      }).then(user => {
        res.redirect(301, '/dashboard');
      })
    })
  })
  
  app.get('/user/update/:id', (req, res) => {
    const userId = req.params.id;
  
    user_game.findOne({
      where: {
        id: userId
      }
    }).then(user => {
      user_game_biodata.findOne({
        where: {
          user_game_id: user.id
        }
      }).then(biodata => {
        res.render('update', { user, biodata })
      })
    })
  })
  
  app.post('/user/update/:id', (req, res) => {
    const userId = req.params.id;
  
    user_game.update({
      name: req.body.name,
      email: req.body.email,
      credit: req.body.credit
    }, {
      where: {
        id: userId
      }
    }).then(user => {
      user_game_biodata.update({
        alamat: req.body.alamat,
        kelamin: req.body.kelamin
      }, {
        where: {
          user_game_id: userId
        }
      }).then(biodata => {
        res.redirect(301, '/dashboard')
      })
    })
  })
  
  app.get('/api/user', (req, res) => {
    user_game.findAll().then(userGame => {
      res.status(200).json(userGame)
    })
  });
  
  app.post('/api/user/update/:id', (req, res) => {
    const userId = req.params.id;
  
    user_game.update({
      name: req.body.name,
      email: req.body.email,
      credit: req.body.credit
    }, {
      where: {
        id: userId
      }
    }).then(user => {
      user_game_biodata.update({
        alamat: req.body.alamat,
        kelamin: req.body.kelamin
      }, {
        where: {
          user_game_id: userId
        }
      }).then(biodata => {
        res.status(200).json({ message: "berhasil update data" })
      })
    })
  })
  
  app.listen(3000, () => console.log('apps berjalan di port 3000'));