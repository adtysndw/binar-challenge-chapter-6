'use strict';
module.exports = (sequelize, DataTypes) => {
  const user_game_biodata = sequelize.define('user_game_biodata', {
    user_game_id: DataTypes.INTEGER,
    alamat: DataTypes.STRING,
    kelamin: DataTypes.STRING
  }, {});
  user_game_biodata.associate = function(models) {
    // associations can be defined here
  };
  return user_game_biodata;
};