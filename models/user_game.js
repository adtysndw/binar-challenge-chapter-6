'use strict';
module.exports = (sequelize, DataTypes) => {
  const user_game = sequelize.define('user_game', {
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    credit: DataTypes.INTEGER
  }, {});
  user_game.associate = function (models) {
    // associations can be defined here
  };
  return user_game;
};