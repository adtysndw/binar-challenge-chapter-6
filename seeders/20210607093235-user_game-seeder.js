'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('user_games', [{
      name: 'Aditya',
      email: 'aditya@gmail.com',
      password: '12345678',
      credit: 200000,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('user_games', null, {});
  }
};
